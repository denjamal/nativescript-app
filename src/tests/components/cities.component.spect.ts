import { CitiesComponent } from "~/app/home/cities/cities.component";
import { NgxsModule, Store } from "@ngxs/store";
import { HomeState } from "~/app/home/state/home.state";
import { TestModuleMetadata, async, ComponentFixture, TestBed } from "@angular/core/testing";
import { nsTestBedBeforeEach, nsTestBedAfterEach, nsTestBedRender } from "nativescript-angular/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { NativeScriptRouterModule } from "nativescript-angular/router";

describe('Cities component', () => {

    const moduleDef: TestModuleMetadata = {
        declarations: [CitiesComponent],
        imports: [
            NgxsModule.forRoot([HomeState]),
            RouterTestingModule,
            NativeScriptRouterModule
        ],
        providers: [
        ]
    }

    beforeEach(nsTestBedBeforeEach(moduleDef.declarations, moduleDef.providers, moduleDef.imports));

    afterEach(nsTestBedAfterEach(true));

    it("the component should be initialized", async(() => {
        nsTestBedRender(CitiesComponent)
            .then((fixture: ComponentFixture<CitiesComponent>) => {
                const component = fixture.componentRef.instance;
                expect(component).toBeDefined();
            });
    }));

    it("onItemTap should dispatch action", async(() => {
        nsTestBedRender(CitiesComponent)
            .then((fixture: ComponentFixture<CitiesComponent>) => {
                const component = fixture.componentRef.instance;
                const store = TestBed.get(Store) as Store;
                spyOn<Store>(store, 'dispatch');

                component.onItemTap(1);

                expect(store.dispatch).toHaveBeenCalledTimes(1);
            });
    }));
});