import { ComponentFixture, async, TestModuleMetadata } from "@angular/core/testing";
import { nsTestBedAfterEach, nsTestBedBeforeEach, nsTestBedRender } from "nativescript-angular/testing";
import { HomeComponent } from "~/app/home/home/home.component";
import { NgxsModule } from "@ngxs/store";
import { HomeState } from "~/app/home/state/home.state";
import { RouterTestingModule } from '@angular/router/testing';
import { NativeScriptRouterModule } from "nativescript-angular/router";

describe("Home component", () => {

    const moduleDef: TestModuleMetadata = {
        declarations: [HomeComponent],
        imports: [
            NgxsModule.forRoot([HomeState]),
            RouterTestingModule,
            NativeScriptRouterModule
        ],
        providers: [
        ]
    }

    beforeEach(nsTestBedBeforeEach(moduleDef.declarations, moduleDef.providers, moduleDef.imports));

    afterEach(nsTestBedAfterEach(true));

    it("the component should be initialized", async(() => {
        nsTestBedRender(HomeComponent)
            .then((fixture: ComponentFixture<HomeComponent>) => {
                const component = fixture.componentRef.instance;
                expect(component).toBeDefined();
            });
    }));
});