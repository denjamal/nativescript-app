import { ComponentFixture, async } from "@angular/core/testing";
import { nsTestBedAfterEach, nsTestBedBeforeEach, nsTestBedRender } from "nativescript-angular/testing";
import { AppComponent } from "~/app/app.component";


describe("App component", () => {

    beforeEach(nsTestBedBeforeEach([
        AppComponent
    ]));

    afterEach(nsTestBedAfterEach(true));

    it("the component should be initialized", async(() => {
        nsTestBedRender(AppComponent)
            .then((fixture: ComponentFixture<AppComponent>) => {
                const component = fixture.componentRef.instance;
                expect(component).toBeDefined();
            });
    }));
});