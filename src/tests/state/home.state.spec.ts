import { CitiesComponent } from "~/app/home/cities/cities.component";
import { NgxsModule, Store } from "@ngxs/store";
import { HomeState } from "~/app/home/state/home.state";
import { TestModuleMetadata, TestBed } from "@angular/core/testing";
import { nsTestBedBeforeEach, nsTestBedAfterEach } from "nativescript-angular/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { CitySelected } from "~/app/home/state/home.actions";
import { City } from "~/app/home/models/home.models";

describe('HomeState', () => {

    const moduleDef: TestModuleMetadata = {
        declarations: [CitiesComponent],
        imports: [
            NgxsModule.forRoot([HomeState]),
            RouterTestingModule,
            NativeScriptRouterModule
        ],
        providers: [
        ]
    };

    let store: Store;

    beforeEach(nsTestBedBeforeEach(moduleDef.declarations, moduleDef.providers, moduleDef.imports));

    beforeEach((done) => {
        store = TestBed.get(Store);
        done();
    });

    afterEach(nsTestBedAfterEach(true));

    it('correctly set state after CitySelected action ', () => {
        const expectedSelectedCity: City = { id: 100, name: 'Sydney', country: 'Australia', currentTemp: '31C' };
        const action = new CitySelected(expectedSelectedCity);

        store.dispatch(action);

        store.selectOnce(HomeState.selectedCity).subscribe(result => {
            expect(result).toBe(expectedSelectedCity);
        });
    });
});