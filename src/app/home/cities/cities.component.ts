import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { City } from '../models/home.models';
import { Store } from '@ngxs/store';
import { CitySelected } from '../state/home.actions';

@Component({
  selector: 'ns-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css'],
  moduleId: module.id,
})
export class CitiesComponent implements OnInit {

  constructor(private page: Page, private store: Store) { }

  cities: City[] = [
    {
      name: 'New-York',
      country: 'USA',
      id: 0,
      currentTemp: '21°C'
    },
    {
      name: 'Khm',
      country: 'Ukraine',
      id: 1,
      currentTemp: '15°C'
    },
    {
      name: 'Tokio',
      country: 'Japan',
      id: 2,
      currentTemp: '30°C'
    }
  ];

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

  onItemTap(id: number) {
    const city = this.cities.find(x => x.id === id);
    this.store.dispatch(new CitySelected(city));
  }
}
