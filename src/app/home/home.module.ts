import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { CitiesComponent } from './cities/cities.component';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { NgxsModule } from '@ngxs/store';
import { HomeState } from './state/home.state';

@NgModule({
  declarations: [HomeComponent, CitiesComponent],
  imports: [
    NativeScriptCommonModule,
    HomeRoutingModule,
    NativeScriptRouterModule,
    NgxsModule.forFeature([HomeState])
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule { }
