export interface City {
    id: number,
    name: string,
    country: string,
    currentTemp: string
}