import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { Select } from '@ngxs/store';
import { HomeState } from '../state/home.state';
import { City } from '../models/home.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'ns-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  moduleId: module.id,
})
export class HomeComponent implements OnInit {

  constructor(private page: Page) { }

  @Select(HomeState.selectedCity) selectedCity$: Observable<City>;

  ngOnInit() {
    this.page.actionBarHidden = true;
  }

}
