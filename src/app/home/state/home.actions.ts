import { City } from "../models/home.models";

export class CitySelected {

    static readonly type = '[Home] City selected';

    constructor(public city: City) { }
}