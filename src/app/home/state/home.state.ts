import { City } from "../models/home.models";
import { State, Selector, Action, StateContext } from "@ngxs/store";
import { CitySelected } from "./home.actions";
import { RouterExtensions } from "nativescript-angular/router";

interface HomeStateModel {
    selectedCity: City
}

@State<HomeStateModel>({
    name: 'home',
    defaults: {
        selectedCity: {
            id: 0,
            name: 'New York',
            country: 'USA',
            currentTemp: '21°C'
        }
    }
})
export class HomeState {

    constructor(private routerExtensions: RouterExtensions) { }

    @Selector()
    static selectedCity(state: HomeStateModel) {
        return state.selectedCity;
    }

    @Action(CitySelected)
    selectCity(ctx: StateContext<HomeStateModel>, action: CitySelected) {
        ctx.patchState({ selectedCity: action.city });
        this.routerExtensions.back();
    }
}